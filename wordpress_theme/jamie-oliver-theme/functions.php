<?php
/**
 * jamie_oliver_theme functions and definitions
 *
 * @package jamie_oliver_theme
 */

/*
 *constants
 */
define('THEME_PREFIX', 'jamie_oliver_theme');
define('THEME_NAME', 'jamie_oliver_theme');
define('TEXT_DOMAIN', 'jamie_oliver_theme');
define('THEME_VERSION', '1.0');
define('THEME_PATH', get_template_directory_uri());
define('SERVER_PATH', get_template_directory());
define('THEME_IMAGES_PATH', get_template_directory_uri().'/images');
global $wpdb;

include(get_template_directory() . '/inc/valitron-master/src/Valitron/Validator.php');

function main_scripts(){
	if ( !is_admin() ) {
	    				
	    //Main style
	    wp_enqueue_style('style.css', THEME_PATH . '/style.css', array(), '', 'all');

		//jquery	
		wp_enqueue_script("jquery", "//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js", array('jquery'), '', true);

		//custom	
		wp_enqueue_script("custom.js", THEME_PATH . "/js/custom.js", array('jquery'), '', true);
	}
}
add_action('wp_enqueue_scripts', 'main_scripts');

// Register Custom Post Type
function recipes() {

	$labels = array(
		'name'                => _x( 'Recipes', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Recipes', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Recipes', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Item:', 'text_domain' ),
		'all_items'           => __( 'All Items', 'text_domain' ),
		'view_item'           => __( 'View Item', 'text_domain' ),
		'add_new_item'        => __( 'Add New Item', 'text_domain' ),
		'add_new'             => __( 'Add New', 'text_domain' ),
		'edit_item'           => __( 'Edit Item', 'text_domain' ),
		'update_item'         => __( 'Update Item', 'text_domain' ),
		'search_items'        => __( 'Search Item', 'text_domain' ),
		'not_found'           => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'recipes', 'text_domain' ),
		'description'         => __( 'Post Type Description', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', 'custom-fields', 'page-attributes', ),
		'hierarchical'        => false,
		'rewrite'             => array( 'slug' => '/recipes' ),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-image-filter',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	register_post_type( 'recipes', $args );

}

// Hook into the 'init' action
add_action( 'init', 'recipes', 0 );


class Recipes{
	
	
	public function __construct() {
		add_action( 'wp_ajax_get_recipes', array( $this, 'ajax_show_recipes' ) );
		add_action( 'wp_ajax_nopriv_get_recipes', array( $this, 'ajax_show_recipes' ) );
		
		add_action( 'wp_ajax_add_new_recipe', array( $this, 'ajax_new_recipes' ) );
		add_action( 'wp_ajax_nopriv_add_new_recipe', array( $this, 'ajax_new_recipes' ) );
    }
	
	
	public function ajax_show_recipes() {
		
		//for filtering
		$order_direction 	= ($_POST['order_direction'] != "") ? $_POST['order_direction'] : 'DESC';
		$order_by 			= ($_POST['order_by'] != "") ? $_POST['order_by'] : 'date';
		$how_many 			= ($_POST['how_many'] != "") ? $_POST['how_many'] : '4';
		
		$args = array(
			'post_type' 		=> 'recipes',
			'post_status' 		=> 'publish',
			'posts_per_page' 	=> 5, 
			//'offset' 			=> 1,
			'showposts' 		=> $how_many,
			'orderby'       	=> $order_by,
			'order' 			=> $order_direction
		);
		$get_recipes = new WP_Query($args);
		$recipes = $get_recipes->found_posts;
		?>
		
		<!-- recipes_list -->
		<div class="recipes_list">
			<?php
			$wrap_div = "<div class='recipes_list'>";
			if( $get_recipes->have_posts() ):
				
				echo $wrap_div;
				while( $get_recipes->have_posts() ): $get_recipes->the_post();
				
					get_template_part('content');
					
				endwhile;
				
				// Close the $wrap_div
				echo '</div>';
				
			else:
				
				get_template_part('content-none');
				
			endif;
			wp_reset_postdata();
			?>
		</div>
		<!-- // recipes_list -->
		<?php
		die();
	}
	
	
	public function insert_recipes($valid, $post_title = '', $post_content = '', $difficulty = '', $cooking_time = '', $ingredients = '', $method_instructions = ''){ 
		
		if ($valid == true) {
			
			//To enter a new post for a custom type
			$post_id_inserted = wp_insert_post(array(
										'post_type' => 'recipes', 
										'post_title' => $post_title,
										'post_content' => $post_content, 
										'post_status' => 'publish', //usually it should be pending, to be approved after
										'comment_status' => 'closed', 
										'ping_status' => 'closed', 
										)
									);
									
			if ($post_id_inserted) {
				$tag = array( $category );
				$taxonomy = 'recipes';
				wp_set_post_terms($post_id_inserted, $tag, $taxonomy );
				add_post_meta($post_id_inserted, 'difficulty', $difficulty);
				add_post_meta($post_id_inserted, 'cooking_time', $cooking_time);
				add_post_meta($post_id_inserted, 'ingredients', $ingredients);
				add_post_meta($post_id_inserted, 'method_instructions', $method_instructions);
				
			}
			echo json_encode(array('status' => 'inserted', 'post_id_inserted' => $post_id_inserted ,'inserted_message'=> 'Recipe inserted with success!<br>An administrator will approve this in the shortest time possible'));
					
				
		}
	}
	
	
	public function ajax_new_recipes() {
		 		
		$valid = true;
		if(isset($_POST)){
			
			if($_POST['post_title'] != ""){
				$post_title = filter_var($_POST['post_title'], FILTER_SANITIZE_STRING);
				$valid == true;
			} else {
				$valid = false;
			} 
			
			if($_POST['post_content'] != ""){
				$post_content = filter_var($_POST['post_content'], FILTER_SANITIZE_STRING);
				$valid == true;
			} else {
				$valid = false;
			} 
			
			if($_POST['difficulty'] != ""){
				$difficulty = filter_var($_POST['difficulty'], FILTER_SANITIZE_STRING);
				$valid == true;
			} else {
				$valid = false;
			} 
			
			if($_POST['cooking_time'] != ""){
				$cooking_time = filter_var($_POST['cooking_time'], FILTER_VALIDATE_INT);
				$valid == true;
			} else {
				$valid = false;
			} 
			
			if($_POST['ingredients'] != ""){
				$ingredients = filter_var($_POST['ingredients'], FILTER_SANITIZE_STRING);
				$valid == true;
			} else {
				$valid = false;
			} 
			
			if($_POST['method_instructions'] != ""){
				$method_instructions = filter_var($_POST['method_instructions'], FILTER_SANITIZE_STRING);
				$valid == true;
			} else {
				$valid = false;
			} 
			
			$this->insert_recipes($valid, $post_title, $post_content, $difficulty, $cooking_time, $ingredients, $method_instructions);
			exit;
		
		}
	}
}

new Recipes();
