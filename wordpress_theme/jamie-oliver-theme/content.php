<div class="recipe">

	<h3>
		<a href="<?php echo get_the_permalink($post->ID); ?>">
		<?php echo $post->post_title; 
		$meta = get_post_meta( $post->ID );
		?>
		</a>
	</h3>
    <ul>
		<li><span>Difficulty: </span><?php echo $meta["difficulty"][0]; ?></li>
		<li><span>Cooking time: </span><?php echo $meta["cooking_time"][0]; ?></li>
		<li><span>Ingredients: </span><?php echo $meta["ingredients"][0]; ?></li>
		<li><span>Method instructions: </span><?php echo $meta["method_instructions"][0]; ?></li>
	</ul>
</div>
