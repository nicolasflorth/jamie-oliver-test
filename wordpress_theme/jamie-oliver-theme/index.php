<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 */

get_header();
?>
<div class="wrapper">
	<form id="filters" method="post" action="">
		<div class="box">
			<label>Order by</label>
			<select name="order_by">
				<option value="date" selected>Date</option>
				<option value="title">Title</option>
			</select>
		</div>
		<div class="box">
			<label>Order direction</label>
			<select name="order_direction">
				<option value="asc" selected>Asc</option>
				<option value="desc">Desc</option>
			</select>
		</div>
		
		<div class="box">
			<label>How many?</label>
			<input name="how_many" type="number" value="2" placeholder="How many to show?" />
		</div>
		
		<input id="filter" type="submit" name="filter" value="Filter" />
		
	</form>
	
	<div class="clear"></div>
	<h1>Recipes</h1>
	<div id="recipes"></div>

	<form id="insertRecipe" method="post" action="">
					
		<input type="hidden" name="add_new_recipe" value="add_new_recipe" />
		
		<div class="box">
			<label>Titlu</label>
			<input name="post_title" type="text" value="New title" placeholder="Title" />
		</div>
		
		<div class="box">
			<label>Content</label>
			<textarea name="post_content" type="textarea" placeholder="Content">New content</textarea>
		</div>
		 
		<div class="box">
			<label>Difficulty</label>
			<select name="difficulty">
				<option value="super-easy" selected>Super easy</option>
				<option value="not-too-tricky">Not too tricky</option>
				<option value="showing-off">Showing off</option>
			</select>
		</div>
		
		<div class="box">
			<label>Cooking Time</label>
			<input name="cooking_time" type="number" value="2" placeholder="Cooking Time" />
		</div>
		
		<div class="box">
			<label>Ingredients</label>
			<input name="ingredients" type="text" value="New ingredient" placeholder="Ingredients" />
		</div>
		
		<div class="box">
			<label>Method/Instructions</label>
			<input name="method_instructions" type="text" value="New method" placeholder="Method/Instructions" />
		</div>
		
		<input type="hidden" name="new_post" value="1" />
		<input id="submit" type="submit" name="submit" value="Submit" />
		
	</form>
	<div id="msgpost"></div>
</div>
<?php get_footer(); ?>
