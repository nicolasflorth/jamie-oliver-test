<?php
/**
 * The Header for this theme.
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0, width=device-width" />
	<title>
		<?php
		/*
		 * Print the <title> tag based on what is being viewed.
		 */
		global $page, $paged;
		wp_title('|', true, 'right');

		// Add the blog description for the home/front page.
		$site_description = get_bloginfo('description', 'display');
		if ($site_description && ( is_home() || is_front_page() ))
			echo " | $site_description";

		// Add a page number if necessary:
		if ($paged >= 2 || $page >= 2)
			echo ' | ' . sprintf(__('Page %s', ''), max($paged, $page));
		?>
	</title>
	
	<!-- Enable IE9 Standards mode -->
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

	