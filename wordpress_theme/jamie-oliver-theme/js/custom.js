﻿(function(){
	"use strict"

	var ajaxurl = 'http://'+window.location.host+'/wp-admin/admin-ajax.php';

	function show_recipes(){

		jQuery.ajax({
			type: 'GET',
			url: ajaxurl,
			data: {
				'action' : 'get_recipes'
			},
			beforeSend: function () {
				//Show loader here
			},
			success: function(data) {
				jQuery("#recipes").html(data);
			},
			error: function() {
				//jQuery("#recipes").html('<p>There has been an error</p>');
			}
		});	
	};
	
	function insert_recipe(){
		jQuery("#submit").on('click', (function(e){
				e.preventDefault();
				jQuery.ajax({
					type: "POST",
					url: ajaxurl,
					data: {
						'action' 				: 'add_new_recipe',
						'post_title' 			: jQuery("input[name='post_title']").val(),
						'post_content' 			: jQuery("textarea[name='post_content']").val(),
						'difficulty' 			: jQuery("select[name='difficulty']").val(),
						'cooking_time' 			: jQuery("input[name='cooking_time']").val(),
						'ingredients' 			: jQuery("input[name='ingredients']").val(),
						'method_instructions' 	: jQuery("input[name='method_instructions']").val(),
					},
					beforeSend: function () {
						//Show loader here
					},
					success: function(data) {
						show_recipes();
					},
					error: function() {
						//jQuery("#recipes").html('<p>There has been an error</p>');
					}
				});
			})
		);
	};
	
	function filter_by(){
		jQuery("#filter").on('click', (function(e){
				e.preventDefault();
				jQuery.ajax({
					type: "POST",
					url: ajaxurl,
					data: {
						'action' 			: 'get_recipes',
						'how_many'	 		: jQuery("input[name='how_many']").val(),
						'order_by'	 		: jQuery("select[name='order_by']").val(),
						'order_direction'	: jQuery("select[name='order_direction']").val()
					},
					beforeSend: function () {
						//Show loader here
					},
					success: function(data) {
						jQuery("#recipes").html(data);
					},
					error: function() {
						//jQuery("#recipes").html('<p>There has been an error</p>');
					}
				});
			})
		);
	};
	
	jQuery(document).ready(function($) {
		
		show_recipes();
		insert_recipe();
		filter_by();

	});

})();