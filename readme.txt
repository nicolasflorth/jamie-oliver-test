AUTHOR
Nicolae Florescu

GIT REPOSITORY
https://nicolasflorth@bitbucket.org/nicolasflorth/jamie-oliver-test.git

HTML-CSS

This part of the project is using npm with gulp
To run this side of the test you need to clone the repository, go into the "html-css" folder, run "npm install" and "gulp" after.
Is using BrowserSync, and is a path setted up into the gulp file saying where from to open the project automatically into the browser after "gulp" command. If the project is not cloned into the same location, then running "gulp" must be done after correcting this path.
I committed to git the "dest" folder too, to can be viewed without running "gulp", but usually, the "dest" folder will be ignored in git.

WORDPRESS THEME

This theme can be uploaded to any WP project
The theme itself is inside the "wordpress_theme" folder. 
Because is no validation done on the adding new recipe form I added some Value to each input and select for fast testing.

I done everything it was requested, but many other things should be done:
- form validation
- more styling
- adding a loader.gif

Usually, I use Advance Custom Fields PRO to create Custom Fields, but this time, I tried to see if they are created directly when they are inserted and it worked.
