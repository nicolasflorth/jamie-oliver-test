(function(){
	"use strict";

	var htmlElements = [],
		currencySign = "&pound;";

	$(document).ready(function($){

		//get the JSON 
	    $.getJSON('https://www.mfortune.co.uk/mk/jackpotservice/',
		{
		  	format: "json"
		}, function(data) {	

	        //print the elements 
		  	$.each( data, function( key, val ) {
		    	htmlElements.push( "<div class='jackpotelement' data-gamepageurl='" + data[key].gamePageURL + "'><img src='" + data[key].gameImage + "'  alt='" + data[key].gameName + "' /><h3 class='title'>" + data[key].gameName + " <span>by mFortune</span></h3><button data-jackpot='" + data[key].gameJackpot + "' data-originalvalue='Show Jackpot'>Show Jackpot</button></div>" );
		  	});
		 
		 	//append the elements of the array to body
		  	$('<div/>', {
		    	'class'	: 'wrapper',
				'id'	: 'jackpotList',
		    	html	: htmlElements.join( '' )
		  	}).appendTo( '#jackpotelements' );

			//check if the element is in the view port. If yes, add a class
			$('.jackpotelement').inViewport(
	            function(){$(this).addClass("in-view");},
	            function(){$(this).removeClass("in-view");}
	        );

			jQuery('.am-in-view').velocity("transition.fadeIn", { stagger: 200 });

		  	//for each element
		    $('.jackpotelement').each(function(){

		    	//redirect when click on image or title to the game page
				$(this).find('img, h3').click(function(){
					var ParentLink = $(this).closest('.jackpotelement').data('gamepageurl');

					//redirect to the game link
					window.open(ParentLink, '_blank');
				});

				//show the jackPot value on click
				$(this).find('button').click(function(){
					
					//get the jackPot value
					var $this 			= $(this),
						jackPotValue 	= $this.data('jackpot'),

						//set jackPotValue to 2 decimals
						jackPotValue2decimals 	= parseFloat(Math.round(jackPotValue * 100) / 100).toFixed(2),

						//get the Game name to can add it to the button
						jackPotGameName 		= $this.closest('.jackpotelement').find('img')[0].getAttribute('alt'),

						//cache the original button value to add it back after a few seconds
						buttonOriginalValue 	= $this.data('originalvalue');

					$this.html(currencySign + jackPotValue2decimals + " " + jackPotGameName + " Jackpot");

					//reverse the button value after 5 seconds
					setTimeout(function(){ 
						$this[0].innerHTML = buttonOriginalValue;
					}, 5000);

				});
			});
		  	
	    });

	});

})();