const gulp 			= require('gulp');
var plug 			= require('gulp-load-plugins')({ lazy: true });

var jshint 			= require('gulp-jshint');
var jshint 			= require('gulp-jshint');
var uglify 			= require('gulp-uglify');
var concat 			= require('gulp-concat');
var sass 			= require('gulp-sass');
var rename 			= require('gulp-rename');
var sourcemaps 		= require('gulp-sourcemaps');
var browserSync 	= require('browser-sync').create();
const stylish 		= require('jshint-stylish');

// ////////////////////////////////////////////////
// Browser-Sync Tasks
// ////////////////////////////////////////////////
gulp.task('browserSync', function() {
    browserSync.init({
        proxy: "http://localhost/tests/jamie-oliver-test/html-css/"
    });
});

// ///////////////////////////////////////////////
// HTML Task
// ///////////////////////////////////////////////
gulp.task('html', function(){
    gulp.src('./*.html')
    .pipe(browserSync.stream());
});

// ///////////////////////////////////////////////
// SASS Task
// ///////////////////////////////////////////////
gulp.task('sass', function () {
    gulp.src('./sass/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./dest'))
		.pipe(rename('all.min.css'))
		.pipe(gulp.dest('./dest'))
		.pipe(browserSync.stream());
});

// ////////////////////////////////////////////////
// Watch tasks
// ////////////////////////////////////////////////
gulp.task('watch', function(){
	gulp.watch('./sass/**/*.scss', ['sass']);
    gulp.watch("jamie-oliver-test/*.html").on('change', browserSync.reload);
});

// ////////////////////////////////////////////////
// Default tasks
// ////////////////////////////////////////////////
gulp.task('default', ['watch', 'browserSync', 'sass']);
